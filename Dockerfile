FROM php:7.2.6-apache
RUN docker-php-ext-install pdo_mysql
RUN apt-get update -y && apt-get install -y sendmail libpng-dev

RUN apt-get update && \
    apt-get install -y \
        zlib1g-dev 
RUN docker-php-ext-install zip

RUN apt-get update

RUN apt-get install -y libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6-dev

RUN docker-php-ext-configure gd --with-gd --with-webp-dir --with-jpeg-dir \
    --with-png-dir --with-zlib-dir --with-xpm-dir --with-freetype-dir \
    --enable-gd-native-ttf
	
	
RUN docker-php-ext-install gd
RUN a2enmod rewrite

COPY www/ /var/www/html/

WORKDIR /var/www/html/

RUN chmod -R 777 /var/www

RUN chmod -R 777 /var/www/html



