# Docker Container for Opencart

## System reqirements

Docker for Desktop / Docker toolbox must be installed.

if you use docker toolbox  please refer this solution for docker volume mount issues

https://stackoverflow.com/questions/57756835/docker-toolbox-volume-mounting-not-working-on-windows-10

## Setup

we can place the opencart files inside "www" 

* On docker-compose.yml  volume mount will be change based on your project structure.
* Navigate to project folder and simply run  "docker-compose up -d --build" this will create our container
* you can start development inside "www"


